import { PaginationFilterDateComponent } from './../pagination-filter-date.component';
import { LanguageLibService } from '@faab/language-lib';
import {
  PaginationEvent,
  PaginationEventData,
  PaginationProcessData,
  PaginationTheadCellBaseComponent
} from '@faab/pagination-lib';
import { Component, OnInit, ElementRef, Input, Output, EventEmitter, ViewChild, Injectable, HostListener } from '@angular/core';
import { NgbDatepickerI18n, NgbDateStruct, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { PaginationFilterDateService } from './../../service/pagination-filter-date.service';
import { DatepickerI18nValue } from '../../interface/datepicker-i18n-value';
import { PaginationEventDateData } from '../../interface/pagination-event-date-data';
import { PaginationEventDatepickerData } from '../../interface/pagination-event-datepicker-data';
import { isArray } from 'util';

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  private i18n: DatepickerI18nValue;

  constructor(
    private lang: LanguageLibService,
    private filterDateService: PaginationFilterDateService
  ) {
    super();
    this.i18n = this.filterDateService.getDatepickerI18n();
  }

  getWeekdayShortName(weekday: number): string {
    const lang_code: string = this.lang.getCurrentLanguage();
    if (isNaN(weekday)) {
      return '';
    }
    const weekday_formatted: number = weekday - 1;
    return (
      isNaN(weekday)
      || !this.i18n
      || !this.i18n[lang_code]
      || !this.i18n[lang_code].weekdays
      || !this.i18n[lang_code].weekdays[weekday_formatted]
    ) ? '' : this.i18n[lang_code].weekdays[weekday_formatted];
  }

  getMonthShortName(month: number): string {
    const lang_code: string = this.lang.getCurrentLanguage();
    if (isNaN(month)) {
      return '';
    }
    const month_formatted: number = month - 1;
    return (
      isNaN(month)
      || !this.i18n
      || !this.i18n[lang_code]
      || !this.i18n[lang_code].months
      || !this.i18n[lang_code].months[month_formatted]
    ) ? '' : this.i18n[lang_code].months[month_formatted];
  }

  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'lib-datepicker-i18n',
  templateUrl: './datepicker-i18n.component.html',
  styles: ['./datepicker-i18n.component.scss'],
  providers: [{provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class DatepickerI18nComponent {
  @ViewChild('datepicker') datepicker: NgbInputDatepicker;
  @Input() title: string;
  @Input() action: 'df' | 'dt';
  @Input() model: NgbDateStruct;
  @Input() class: string;
  @Input() placement: string = 'bottom-right';
  @Output() dateChange: EventEmitter<PaginationEventDatepickerData> = new EventEmitter<PaginationEventDatepickerData>();

  id: NgbInputDatepicker;

  @HostListener('document:click', ['$event']) clickOut(e: MouseEvent) {
    if (!(e instanceof MouseEvent)) {
      return;
    }
    this.onClick(e);
  }
  constructor(
    private _eref: ElementRef,
    private datepickerService: PaginationFilterDateService
  ) {}

  open() {
    this.openDatepicker(this.datepicker);
  }

  getId(): NgbInputDatepicker {
    return this.datepickerService.dynamicId;
  }

  setId(id: NgbInputDatepicker): void {
    this.datepickerService.dynamicId = id;
    this.id = id;
  }


  openDatepicker(id: NgbInputDatepicker): void {
    const new_id: NgbInputDatepicker = this.getId();
    if (new_id === id) {
      id.close();
      this.setId(null);
      return;
    }
    if (new_id) {
      new_id.close();
    }
    this.setId(id);
    id.open();
  }


  onClick(e: MouseEvent): void {
    if (!(e instanceof MouseEvent)) {
      return;
    }
    if (!this.id) {
      return;
    }
    if (
      !this._eref.nativeElement.contains(e.target)
      && !e.target['classList'].contains('datapicker-ctrl-open')
    ) {
      this.id.close();
      this.setId(null);
    }
  }

  select() {
    // debugger;
    if (
      !isArray(PaginationFilterDateComponent.PARAM_NAME)
      || PaginationFilterDateComponent.PARAM_NAME.indexOf(this.action) < 0
      || !this.model
    ) {
      return;
    }
    this.dateChange.emit({
      type: this.action,
      value: this.model
    } as PaginationEventDatepickerData);
  }

  clear() {
    this.model = undefined;
    this.select();
  }

  getClassName(): string {
    return (this.class) ? this.class : '';
  }
}
