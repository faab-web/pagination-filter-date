import { PaginationLibComponent } from '@faab/pagination-lib';
import { isArray, isString, isNumber } from 'util';
import {
  PaginationEvent,
  PaginationEventData,
  PaginationProcessData,
  PaginationTheadCellBaseComponent,
  PaginationParamItem
} from '@faab/pagination-lib';
import { Component, OnInit, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { PaginationEventDateData } from '../interface/pagination-event-date-data';
import { PaginationEventDateValueData } from '../interface/pagination-event-date-value-data';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { PaginationEventDatepickerData } from '../interface/pagination-event-datepicker-data';

@Component({
  selector: 'lib-pagination-filter-date',
  templateUrl: './pagination-filter-date.component.html',
  styles: ['./pagination-filter-date.component.scss']
})
export class PaginationFilterDateComponent extends PaginationTheadCellBaseComponent {

  static EVENT_TYPE = 'DATE';
  static EVENT_VALUE: PaginationEventDateData[] = [];
  static PARAM_NAME: string[] = ['df', 'dt'];

  /* @Input() date = {
    from: null,
    to: null
  }; */
  // @Output() dateChange: EventEmitter<PaginationEvent> = new EventEmitter<FilterEvent>();
  // @Input() title: string = 'pagination-filter-date.created';

  @Input() value: any = {
    from: undefined,
    to: undefined
  } as PaginationEventDateValueData;

  @ViewChild('cnt') container: ElementRef;
  placement = 'bottom-right';

  constructor() {
    super();
  }

  /* case 'from':
        try {
          const date = new Date(event.data.year, event.data.month - 1, event.data.day);
          this.from = date.getTime();
        } catch (e) {
          return;
        }
        break;
      case 'to':
        try {
          const date = new Date(event.data.year, event.data.month - 1, event.data.day);
          this.to = date.getTime() + 3600 * 24 * 1000;
        } catch (e) {
          return;
        }
        break; */

  public static updateEventData(event_data: PaginationEventData, process_data?: PaginationProcessData): number {
    if (
      !event_data
      || !event_data.key
      || !isString(event_data.key)
      || !event_data['type']
      || !isString(event_data['type'])
      || !event_data['value']
      || !isNumber(event_data['value'].year)
      || !isNumber(event_data['value'].month)
      || !isNumber(event_data['value'].day)
    ) {
      return 1;
    }

    const new_date_collection: PaginationEventDateData[] = [],
      data: PaginationEventDateData = {
        key: event_data.key,
        type: event_data['type'],
        value: event_data['value']
      },
      date_collection_items: PaginationEventDateData[] =  isArray(PaginationFilterDateComponent.EVENT_VALUE) ?
        PaginationFilterDateComponent.EVENT_VALUE : [];
    let is_new: boolean = true;

    date_collection_items.forEach(
      (date_collection_item: PaginationEventDateData) => {
        if (
          isString(date_collection_item.key)
          && isString(date_collection_item.type)
          && date_collection_item.value
        ) {
          if (
            date_collection_item.key === data.key
            && date_collection_item.type === data.type
            && data.value
          ) {
            date_collection_item.value = data.value;
            is_new = false;
          }
          if (date_collection_item.value) {
            new_date_collection.push(date_collection_item);
          }
        }
      }
    );
    if (is_new) {
      new_date_collection.push({
        key: data.key,
        type: data.type,
        value: data.value
      } as PaginationEventDateData);
    }

    PaginationFilterDateComponent.EVENT_VALUE = new_date_collection.length > 0 ?
      new_date_collection : [];
    return 1;
  }

  public static updateEventDataByParamItem(param_item: PaginationParamItem): void {

    /* case 'from':
            this.from = parseInt(p[1], 10);
            break;
          case 'to': */

    const param_name_arr: string[] = isArray(PaginationFilterDateComponent.PARAM_NAME) ? PaginationFilterDateComponent.PARAM_NAME : [];
    if (
      !param_item
      || param_name_arr.indexOf(param_item.name) < 0
      || !param_item.value
    ) {
      return;
    }

    const param_values: string[] = param_item.value.split(PaginationLibComponent.SEPARATOR_MANY_VALUE);
    param_values.forEach(
        // "whenCreated=12345678"
        (param_item_value_string: string) => {
          const param_item_value_array: string[] = param_item_value_string.split(PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM),
            date_integer: number = parseInt(param_item_value_array[1], 10);
          if (isNumber(date_integer)) {
            const convert = new Date();
            convert.setTime(date_integer);
            PaginationFilterDateComponent.updateEventData({
              key: param_item_value_array[0],
              type: param_item.name,
              value: {
                year: convert.getFullYear(),
                month: convert.getMonth() + 1,
                day: convert.getDate()
              } as NgbDateStruct
            } as PaginationEventDateData);
          }
          // self.genFilterField(value);
        }
      );
  }

  public static getParamItem(): string[] {
    const event_values: PaginationEventDateData[] = isArray(PaginationFilterDateComponent.EVENT_VALUE) ?
      PaginationFilterDateComponent.EVENT_VALUE : [],
      event_values_length: number = event_values.length;
    let event_values_from_string: string = '',
      event_values_to_string: string = '';

      /* for (let i = 0; i < event_values_length; i++) {
        const event_value: PaginationFilterTextEventValue = event_values[i];
        if (event_value.value) {
          event_values_arr.push(event_value.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM + event_value.value);
        }
      }

      return (event_values_arr) ? [PaginationFilterTextComponent.PARAM_NAME[0]
        + PaginationLibComponent.SEPARATOR_VALUE + event_values_arr.join(PaginationLibComponent.SEPARATOR_MANY_VALUE)] : []; */

    for (let i = 0; i < event_values_length; i++) {
      const event_value: PaginationEventDateData = event_values[i];
      if (
        event_value.value
        && isNumber(event_value.value.year)
        && isNumber(event_value.value.month)
        && isNumber(event_value.value.day)
      ) {
        const date: Date = new Date(event_value.value.year, event_value.value.month - 1, event_value.value.day);
        switch (event_value.type) {
          case PaginationFilterDateComponent.PARAM_NAME[0]:
            event_values_from_string += event_value.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM
              + date.getTime();
            break;
          case PaginationFilterDateComponent.PARAM_NAME[1]:
            event_values_to_string += event_value.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM
              + date.getTime();
            break;
        }
      }
    }
    const params: string[] = [];
    if (event_values_from_string) {
      params.push(PaginationFilterDateComponent.PARAM_NAME[0] + PaginationLibComponent.SEPARATOR_VALUE + event_values_from_string);
    }
    if (event_values_to_string) {
      params.push(PaginationFilterDateComponent.PARAM_NAME[1] + PaginationLibComponent.SEPARATOR_VALUE + event_values_to_string);
    }
    return params;
  }

  public static generateRequestUrl(): string {
    const filter_collection_items: PaginationEventDateData[] = isArray(PaginationFilterDateComponent.EVENT_VALUE) ?
    PaginationFilterDateComponent.EVENT_VALUE : [];
    let url: string = '';
    filter_collection_items.forEach(
      (filter_collection_item: PaginationEventDateData) => {
        if (
          filter_collection_item.value
          && isNumber(filter_collection_item.value.year)
          && isNumber(filter_collection_item.value.month)
          && isNumber(filter_collection_item.value.day)
        ) {
          const date: Date = new Date(
            filter_collection_item.value.year,
            filter_collection_item.value.month - 1,
            filter_collection_item.value.day
          );

          switch (filter_collection_item.type) {
            case PaginationFilterDateComponent.PARAM_NAME[0]:
              url += '&' + PaginationFilterDateComponent.PARAM_NAME[0] + PaginationLibComponent.SEPARATOR_VALUE
                + filter_collection_item.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM + date.getTime();
              break;
            case PaginationFilterDateComponent.PARAM_NAME[1]:
              url += '&' + PaginationFilterDateComponent.PARAM_NAME[1] + PaginationLibComponent.SEPARATOR_VALUE
                + filter_collection_item.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM + date.getTime();
              break;
            /* if (filter_collection_item.type === PaginationFilterDateComponent.PARAM_NAME[0]) {
              url += PaginationFilterDateComponent.PARAM_NAME[0] + PaginationLibComponent.SEPARATOR_VALUE
                + filter_collection_item.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM
                + new Date(filter_collection_item.value.year, filter_collection_item.value.month - 1, filter_collection_item.value.day);
            } */
            /* if (filter_collection_item.type === PaginationFilterDateComponent.PARAM_NAME[1]) {
              url += PaginationFilterDateComponent.PARAM_NAME[1] + PaginationLibComponent.SEPARATOR_VALUE
                + filter_collection_item.key + PaginationLibComponent.SEPARATOR_FILTER_COLLECTION_ITEM
                + new Date(filter_collection_item.value.year, filter_collection_item.value.month - 1, filter_collection_item.value.day);
            } */
          }
        }
      }
    );
    return url;
  }

  public static getValueByKey(key: string): any {
    // console.log('getValueByKey => (key: ' + key + ')');
    const value: PaginationEventDateValueData = {
      from: undefined,
      to: undefined
    };
    if (!key || typeof key !== 'string') {
      return value;
    }
    const filter_collection_items: PaginationEventDateData[] = isArray(PaginationFilterDateComponent.EVENT_VALUE) ?
      PaginationFilterDateComponent.EVENT_VALUE : [],
      filter_collection_items_length: number = filter_collection_items.length;

    for (let i = 0; i < filter_collection_items_length; i++) {
      const filter_collection_item: PaginationEventDateData = filter_collection_items[i];
      if (
        !filter_collection_item
        || !filter_collection_item.key
        || filter_collection_item.key !== key
        || !filter_collection_item.value
        || !isNumber(filter_collection_item.value.year)
        || !isNumber(filter_collection_item.value.month)
        || !isNumber(filter_collection_item.value.day)
      ) {
        continue;
      }
      /* const date: Date = new Date(
        filter_collection_item.value.year,
        filter_collection_item.value.month - 1,
        filter_collection_item.value.day
      ),
        date_string: string = date.getTime().toString(); */
      switch (filter_collection_item.type) {
        case PaginationFilterDateComponent.PARAM_NAME[0]:
          value.from = filter_collection_item.value;
          break;
        case PaginationFilterDateComponent.PARAM_NAME[1]:
          value.to = filter_collection_item.value;
          break;
      }
    }
    console.log('value: ');
    console.log(value);
    return value;
  }

  onInit(): void {
    super.onInit();
    const rect: DOMRect = this.container.nativeElement.getBoundingClientRect();
    if (rect.x < 400) {
      this.placement = 'bottom-left';
    }
  }

  onDestroy(): void {
    super.onInit();
  }

  afterViewInit(): void {
    super.afterViewInit();
  }

  select(e: PaginationEventDatepickerData): void {
    if (
      !e
      || !isString(this.key)
      || !isArray(PaginationFilterDateComponent.PARAM_NAME)
      || PaginationFilterDateComponent.PARAM_NAME.indexOf(e.type) < 0
      || !e.value
    ) {
      return;
    }
    this.changePaginationEvent.emit(
      new PaginationEvent(
        PaginationFilterDateComponent.EVENT_TYPE,
        {
          key: this.key,
          value: e.value,
          type: e.type
        } as PaginationEventDateData
      )
    );
  }

  public debugEventValue(): any {
    return PaginationFilterDateComponent.EVENT_VALUE;
  }



}
