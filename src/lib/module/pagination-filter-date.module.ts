import { DatepickerI18nComponent } from './../component/datepicker-i18n/datepicker-i18n.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { PaginationFilterDateComponent } from '../component/pagination-filter-date.component';
import { PaginationLibModule } from '@faab/pagination-lib';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationFilterDateModuleOptions } from '../interface/pagination-filter-date-module-options';
import { PAGINATION_FILTER_DATE_OPTIONS_TOKEN } from '../const/pagination-filter-date-options-token';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DatepickerI18nComponent,
    PaginationFilterDateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    NgbDatepickerModule,
    PaginationLibModule
  ],
  entryComponents: [
    PaginationFilterDateComponent
  ],
  exports: [
    PaginationFilterDateComponent
  ]
})
export class PaginationFilterDateModule {
  static forRoot(options?: PaginationFilterDateModuleOptions): ModuleWithProviders {
    return {
      ngModule: PaginationFilterDateModule,
      providers: [
        {
          provide: PAGINATION_FILTER_DATE_OPTIONS_TOKEN,
          useValue: options,
        }
      ]
    };
  }
}
