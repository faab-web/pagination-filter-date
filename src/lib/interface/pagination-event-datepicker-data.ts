import { PaginationEventDateValueData } from './pagination-event-date-value-data';
import { PaginationEventData } from '@faab/pagination-lib';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export interface PaginationEventDatepickerData {
  type: string;
  value: NgbDateStruct;
}
