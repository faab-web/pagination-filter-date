export interface DatepickerI18nValue {
  [lang: string]: {
    weekdays: string[];
    months: string[];
  };
}
