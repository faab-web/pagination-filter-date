import { PaginationEventDateValueData } from './pagination-event-date-value-data';
import { PaginationEventData } from '@faab/pagination-lib';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { PaginationEventDatepickerData } from './pagination-event-datepicker-data';

export interface PaginationEventDateData extends PaginationEventData, PaginationEventDatepickerData {}
