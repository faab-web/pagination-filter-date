import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export interface PaginationEventDateValueData {
  from: NgbDateStruct;
  to: NgbDateStruct;
}
