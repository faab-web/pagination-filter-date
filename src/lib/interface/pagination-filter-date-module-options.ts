import { DatepickerI18nValue } from './datepicker-i18n-value';

export interface PaginationFilterDateModuleOptions {
  datepicker_i18n: DatepickerI18nValue;
}
