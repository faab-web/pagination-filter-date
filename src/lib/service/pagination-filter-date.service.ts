import { PAGINATION_FILTER_DATE_OPTIONS_TOKEN } from './../const/pagination-filter-date-options-token';
import { Injectable, Inject } from '@angular/core';
import { PaginationFilterDateModuleOptions } from '../interface/pagination-filter-date-module-options';
import { DatepickerI18nValue } from './../interface/datepicker-i18n-value';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class PaginationFilterDateService {
  private datepicker_i18n: DatepickerI18nValue;
  dynamicId: NgbInputDatepicker;
  constructor(
    @Inject(PAGINATION_FILTER_DATE_OPTIONS_TOKEN) private token_config: PaginationFilterDateModuleOptions,
  ) {
    const datepicker_i18n: DatepickerI18nValue =  {
      'en': {
        weekdays: ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'],
        months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      },
      'ru': {
        weekdays: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        months: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
      }
    };
    this.datepicker_i18n = (token_config && token_config.datepicker_i18n) ? token_config.datepicker_i18n : datepicker_i18n;
  }

  getDatepickerI18n(): DatepickerI18nValue {
    return this.datepicker_i18n;
  }
}
