import { InjectionToken } from '@angular/core';
import { PaginationFilterDateModuleOptions } from '../interface/pagination-filter-date-module-options';

export const PAGINATION_FILTER_DATE_OPTIONS_TOKEN
  = new InjectionToken<PaginationFilterDateModuleOptions>('PAGINATION_FILTER_DATE_OPTIONS_TOKEN');
